<?php

  require 'dbConnection.php';
  session_start();
  include 'functions.php';

  getUniqueUsers();
  getProjects();

?>

<html>
  <head>
    <title>CapEx Tracking: Add</title>
    <?php include 'headerContent.php'; ?>

    <!--DatePicker jQuery UI -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <!-- MultiDatesPicker API -->
    <script type="text/javascript" src="MultiDatesPicker v1.6.3/js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="MultiDatesPicker v1.6.3/js/jquery-ui-1.11.1.js"></script>
    <script type="text/javascript" src="MultiDatesPicker v1.6.3/jquery-ui.multidatespicker.js"></script>
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="destroy.php"><img src="https://www.languageline.com/images/languageline-logo.png"> Language Line Solutions</a>
        </div>
      </div><!--/.navbar-collapse -->
    </nav>
    <div class="page_content">

      <form action="actionPages/hoursAdd.php" method="POST" id="addForm" name="addForm">
        <div id="datepicker">Date(s) <input id="display-dates" name="dates"/>
          <script>
            $( "#display-dates" ).multiDatesPicker();
          </script>

          Hours <input type="text" name = "hours" size="15">

          Project
          <select name="project">
            <option value="blank"></option>
              <??><?php
                $projects = getProjects();
                foreach ($projects as $project){
                  echo '<option value="'.$project['name'].'">'.$project['name'].'</option>';
                }
              ?>
          </select>

          Activity <input type="text" name = "activity" size="15">

          <input type="submit" value="Add" id="submitMe"/>
        </div>
      </form>

      <!-- DataTables CSS -->
      <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">

      <!-- jQuery -->
      <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>

      <!-- DataTables -->
      <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.js"></script>


      <div id="display" class="tracker_table_add" style="display: none;">
        <h3 style="padding-bottom: 1.5em;"><strong>Previous Entries:</strong></h3>
        <table id="table_id" class="display">
          <thead>
              <tr>
                <th>Edit</th>
                <th>Name</th>
                <th>Date</th>
                <th>Hours</th>
                <th>Project</th>
                <th>Activity</th>
              </tr>
          </thead>
          <tbody>
              <??><?php
                $userInfo = getUniqueUsers();
                foreach ($userInfo as $user){
                  $date = date('m/d/Y', $user['date']);
                  ?>
                  <tr>
                    <form action="edit.php" method="POST">
                      <td><input type="submit" value="  " name="updateForm"/></td>
                      <td><?= $user['nn']?></td>
                      <input type="hidden" name="name" value="<?=$user['nn']?>" />
                      <td><?=$date?></td>
                      <input type="hidden" name="date" value="<?=$user['date']?>" />
                      <td><?= $user['hours']?></td>
                      <input type="hidden" name="hours" value="<?=$user['hours']?>" />
                      <td><?=$user['pn']?></td>
                      <input type="hidden" name="project" value="<?=$user['pn']?>" />
                      <td><?=$user['activity']?></td>
                      <input type="hidden" name="activity" value="<?=$user['activity']?>" />
                    </form>
                  </tr>
                  <?php
                }
              ?>
          </tbody>

          <script>
            $(document).ready( function () {
              $('#table_id').dataTable( {
                "pageLength": 25 ,
                "columnDefs": [
                  { "width": "10%", "targets": 0 }
                ]});
              $('#display').show();
            });
          </script>
        </table>

        <form action="actionPages/searchExport.php" method="POST">
          <input type="hidden" name="exportFrom" value="add" />
          <input type="submit" value="Export" class="btn btn-primary"id="addExportButton"/>
        </form>
      </div>
    </div>
  </body>
</html>
