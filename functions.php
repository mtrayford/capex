<??><?php

function getInfo(){
    $dbConn = getConnection();
    $sql = "SELECT t.hours, t.activity, t.date, p.name as pn, n.name as nn, p.status
      FROM time AS t join projects AS p ON t.project_id = p.project_id
    	JOIN names as n ON t.name_id = n.name_id WHERE p.status = 1 ORDER BY t.date DESC";
      $stmt = $dbConn->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll();
    }

function getUsers(){
    $dbConn = getConnection();
    $sql = "SELECT name FROM names ORDER BY name ASC";
      $stmt = $dbConn->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll();
}


function getUniqueUsers(){
    $dbConn = getConnection();
    $user = $_SESSION['name'];
    $sql = "SELECT t.hours, t.activity, t.date, p.name as pn, n.name as nn
      FROM time AS t join projects AS p ON t.project_id = p.project_id
	    JOIN names as n ON t.name_id = n.name_id WHERE n.name = :user AND p.status = 1 ORDER BY t.date DESC";
    $namedPara = array();
    $namedPara[':user'] = $user;
      $stmt = $dbConn->prepare($sql);
      $stmt->execute($namedPara);
      return $stmt->fetchAll();
}

function getProjects(){
  $dbConn = getConnection();
  $sql = "SELECT name FROM projects WHERE status = 1 ORDER BY name ASC";
    $stmt = $dbConn->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}

function getNameId($name){
  $dbConn = getConnection();
  $sql = "SELECT name_id FROM names WHERE names.name = :name";
  $namedPara = array();
  $namedPara[':name'] = $name;
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($namedPara);
    return $stmt->fetch();
}

function getEntryId($n, $d){
  $dbConn = getConnection();
  $sql = "SELECT id FROM time WHERE time.name_id = :n AND time.date = :d";
  $namedPara = array();
  $namedPara[':n'] = $n;
  $namedPara[':d'] = $d;
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($namedPara);
    return $stmt->fetch();
}

function getProjectId($project){
  $dbConn = getConnection();
  $sql = "SELECT project_id FROM projects WHERE projects.name = :project";
  $namedPara = array();
  $namedPara[':project'] = $project;
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($namedPara);
    return $stmt->fetch();
}

function getLogInfo(){
    $dbConn = getConnection();
    $sql = "SELECT * FROM add_logs ORDER BY time DESC";
      $stmt = $dbConn->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll();
}

function getPieData($i){
  $dbConn = getConnection();
  $sql = "SELECT p.name, SUM(hours),  (MONTH(NOW()) - :i) as month FROM time AS t JOIN projects AS p ON p.project_id = t.project_id
    WHERE (YEAR(FROM_UNIXTIME(date)) = YEAR(NOW()) AND MONTH(FROM_UNIXTIME(date)) = (MONTH(NOW())-:i ))
    GROUP BY t.project_id ORDER BY SUM(hours)  DESC";
  $namedPara = array();
  $namedPara[':i'] = $i;
      $stmt = $dbConn->prepare($sql);
      $stmt->execute($namedPara);
      return $stmt->fetchAll();
}

function getFilteredEntries(){
    $dbConn = getConnection();
    $user = $_SESSION['porp'] . '%';
    $sql = "SELECT n.name as Name, t.date as Date, t.hours as Hours, p.name as Project, t.activity as Activity
          FROM time AS t join projects AS p ON t.project_id = p.project_id
          JOIN names as n ON t.name_id = n.name_id AND (p.name LIKE :porp OR n.name LIKE :porp OR t.activity LIKE :porp)
          WHERE p.status = 1 ORDER BY t.date DESC";
    $namedPara = array();
    $namedPara[':porp'] = $user;
      $stmt = $dbConn->prepare($sql);
      $stmt->execute($namedPara);
      return $stmt->fetchAll();
}

function getEntries(){
  $dbConn = getConnection();
  $sql = "SELECT n.name as Name, t.date as Date, t.hours as Hours, p.name as Project, t.activity as Activity
        FROM time AS t join projects AS p ON t.project_id = p.project_id
        JOIN names as n ON t.name_id = n.name_id AND n.name = :name WHERE p.status = 1 ORDER BY t.date DESC";
  $namedPara = array();
  $namedPara[':name'] = $_SESSION['name'];
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($namedPara);
    return $stmt->fetchAll();
}

?>
