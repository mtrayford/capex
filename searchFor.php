<?php

  require 'dbConnection.php';
  include 'functions.php';
  session_start();
  getInfo();

?>

<!DOCTYPE html>
<html>
  <head>
    <title>CapEx Tracking: Search</title>
    <?php include 'headerContent.php'; ?>
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="destroy.php"><img src="https://www.languageline.com/images/languageline-logo.png"> Language Line Solutions</a>
        </div>
      </div><!--/.navbar-collapse -->
    </nav>

    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">

    <!-- jQuery -->
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>

    <!-- DataTables -->
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.js"></script>

    <div id="display" class="tracker_table_search" style="display: none;">
      <table id="table_id" class="display">
        <thead>
          <tr>
            <th>Edit</th>
            <th>Name</th>
            <th>Date</th>
            <th>Hours</th>
            <th>Project</th>
            <th>Activity</th>
          </tr>
        </thead>
        <tbody>
          <??><?php
            $userInfo = getInfo();
            if(isset($_SESSION['porp'])){
              $porp = $_SESSION['porp'];
            }
            else {
              $porp = "";
            }
            foreach ($userInfo as $user){
              $date = date('m/d/Y', $user['date']);
            ?>
              <tr>
                <form action="edit.php" method="POST">
                  <td><input type="submit" value="  " name="updateForm"/></td>
                  <td><?= $user['nn']?></td>
                  <input type="hidden" name="name" value="<?=$user['nn']?>" />
                  <td><?=$date?></td>
                  <input type="hidden" name="date" value="<?=$user['date']?>" />
                  <td><?= $user['hours']?></td>
                  <input type="hidden" name="hours" value="<?=$user['hours']?>" />
                  <td><?=$user['pn']?></td>
                  <input type="hidden" name="project" value="<?=$user['pn']?>" />
                  <td><?=$user['activity']?></td>
                  <input type="hidden" name="activity" value="<?=$user['activity']?>" />
                </form>
              </tr>
            <?php
            }
          ?>
        </tbody>

        <script>
          var the_porp = "<?= $porp; ?>";
          $(document).ready( function () {
            var myTable = $('#table_id').dataTable( {
              "pageLength": 25,
              "order": [[1, "desc"]],
              "columnDefs": [
                { "width": "10%", "targets": 0 }
              ],
              "dom ": 'T<"clear">lfrtip',
              "tableTools": {
              "sSwfPath": "//cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf",
                "aButtons" : ["xls"]
          }
              });
            myTable.fnFilter(the_porp);
            $('#display').show();
          });
        </script>
      </table>

      <form action="actionPages/searchExport.php" method="POST">
        <input type="hidden" name="exportFrom" value="search" />
        <input type="submit" value="Export" class="btn btn-primary"id="searchExportButton"/>
      </form>
    </div>
  </body>
</html>
