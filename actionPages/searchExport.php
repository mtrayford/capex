<?php

  require '../dbConnection.php';
  require '../functions.php';
  session_start();

  if($_POST['exportFrom'] == "search"){
    $entries = getFilteredEntries();
  }
  else {
    $entries = getEntries();
  }
  
  $file = "Capital_Tracking_".date('m-d-Y',strtotime("now")). '.csv';

  $fp = fopen($file, "w");

  //get column names
  foreach($entries as $row) {
      $line = array();
      $count = 0;
      foreach ($row as $name => $value) {
        if (!preg_match('/[[:digit:]]+/i', $name)) {
          $line[$count] = $name;
        }
        $count += 1;
      }
      fputcsv($fp, array_values($line));
      break;
  }

  //get data
  foreach($entries as $row) {
      $line = array();
      $count = 0;
      foreach ($row as $name => $value) {
        if (!preg_match('/[[:digit:]]+/i', $name)) {
          if($name == "Date"){
            $value = date('m/d/Y', $value);
          }
            $line[$count] = $value;
        }
        $count += 1;
      }
      fputcsv($fp, array_values($line));
  }

  fclose($fp);

  if (file_exists($file)) {
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename='.basename($file));
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($file));
      readfile($file);

  }

  unlink($file);

?>
