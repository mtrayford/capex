<??>
<?php
  require '../dbConnection.php';
  $dbConn = getConnection();

  if($_POST['add_project'] != ""){
    session_start();
    $_SESSION['add_project'] = $_POST['add_project'];
    $project = $_SESSION['add_project'];
    $type = "np";
    // np = New Project

    $sql = "INSERT INTO projects (name) VALUES (:project)";
    $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $namedPara = array();
    $namedPara[':project'] = $project;
        $stmt = $dbConn->prepare($sql);
        $stmt->execute($namedPara);

    $sql = "INSERT INTO add_logs (type, add_name) VALUES (:type, :project)";
    $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $namedPara = array();
    $namedPara[':type'] = $type;
    $namedPara[':project'] = $project;
        $stmt = $dbConn->prepare($sql);
        $stmt->execute($namedPara);
  }

  $url = "index.php";
  header("Location: ../". urlencode($url));
?>
