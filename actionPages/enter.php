
<??>
<?php
  session_start();
  $searchUrl = "searchFor.php";
  $addUrl = "addTo.php";

  if ($_POST['name'] == "blank" && empty($_POST['porp'])) {
    header("Location: ../" . urlencode($searchUrl));
  }

  elseif ($_POST['name'] != "blank" && empty($_POST['porp'])) {
    $_SESSION['name'] = $_POST['name'];
    header("Location: ../" . urlencode($addUrl));
  }

  elseif ($_POST['name'] == "blank" && !empty($_POST['porp'])){
    $_SESSION['porp'] = $_POST['porp'];
    header("Location: ../" . urlencode($searchUrl));
}

?>
