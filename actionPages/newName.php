<??>
<?php
  require '../dbConnection.php';
  $dbConn = getConnection();

  if($_POST['add_name'] != ""){
    session_start();
    $_SESSION['add_name'] = $_POST['add_name'];
    $name = $_SESSION['add_name'];
    $type = "ne";
    //ne = New Employee

    $sql = "INSERT INTO names (name) VALUES (:name)";
    $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $namedPara = array();
    $namedPara[':name'] = $name;
        $stmt = $dbConn->prepare($sql);
        $stmt->execute($namedPara);

    $sql = "INSERT INTO add_logs (type, add_name) VALUES (:type, :name)";
    $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $namedPara = array();
    $namedPara[':type'] = $type;
    $namedPara[':name'] = $name;
       $stmt = $dbConn->prepare($sql);
       $stmt->execute($namedPara);

  }

  $url = "index.php";
  header("Location: ../" . urlencode($url));
?>
