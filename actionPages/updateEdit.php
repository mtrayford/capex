<??>
<?php
  require '../dbConnection.php';
  include '../functions.php';
  $dbConn = getConnection();
  session_start();

  $nameId = getNameId($_POST['name']);
  $newDate = $_POST['new_dates'];
  if(empty($_POST['new_hours'])){
    $newHours = $_SESSION['hours'];
  }
  else{
    $newHours = $_POST['new_hours'];
  }

  $newProject = getProjectId($_POST['new_project']);

  if(empty($_POST['new_activity'])){
    $newActivity = $_SESSION['activity'];
  }
  else{
    $newActivity = $_POST['new_activity'];
  }

  $entryId = getEntryId($nameId[0], $_SESSION['date']);

  $newDate = DateTime::createFromFormat('m/d/Y', $newDate);
  $date_list[] = $newDate->getTimestamp();

  $sql = "UPDATE time as t SET t.date = :newDate, t.hours = :newHours, t.project_id = :newProject, t.activity = :newActivity  WHERE t.id = :entryId";
  $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $namedPara = array();
  $namedPara[':newDate'] = $date_list[0];
  $namedPara[':newHours'] = $newHours;
  $namedPara[':newProject'] = $newProject[0];
  $namedPara[':newActivity'] = $newActivity;
  $namedPara[':entryId'] = $entryId[0];
      $stmt = $dbConn->prepare($sql);
      $stmt->execute($namedPara);

?>

<script language=javascript>window.history.go(-2);</script>
