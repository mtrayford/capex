<??><?php

  include '../functions.php';
  require '../dbConnection.php';
  $dbConn = getConnection();

  session_start();

  $date = $_POST['dates'];
  $dates = explode(", ", $date);
  $date_list = array();

  foreach ($dates as $date){
    $date = DateTime::createFromFormat('m/d/Y', $date);
    $date_list[] = $date->getTimestamp();
  }

  $name = $_SESSION['name'];
  $hours = $_POST['hours'];
  $project = $_POST['project'];
  $activity = $_POST['activity'];

  $name_id = getNameId($name);
  $project_id = getProjectId($project);

  for($i = 0; $i < count($date_list); $i++){
    $sql = "INSERT INTO time (name_id, hours, project_id, activity, date)
      VALUES (:name_id, :hours, :project_id, :activity, :date)";
    $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $namedPara = array();
    $namedPara[':name_id'] = $name_id[0];
    $namedPara[':hours'] = $hours;
    $namedPara[':project_id'] = $project_id[0];
    $namedPara[':activity'] = $activity;
    $namedPara[':date'] = $date_list[$i];
      $stmt = $dbConn->prepare($sql);
      $stmt->execute($namedPara);
  }

  $url = "addTo.php";
  header("Location: ../" . urlencode($url));

?>
