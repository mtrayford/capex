<??><?php

require '../dbConnection.php';
session_start();
include '../functions.php';

getProjects();
getLogInfo();

?>

<html>
  <head>
    <title>CapEx Tracking: Add</title>
     <?php include '../headerContent.php'; ?>
      <link rel="stylesheet" href="../css/styles.css">
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.php"><img src="https://www.languageline.com/images/languageline-logo.png"> Language Line Solutions</a>
        </div>
      </div><!--/.navbar-collapse -->
    </nav>
    <h1 style="padding-top: 3em; padding-left: 1em; "><strong>Welcome Admin!</strong></h1>
    <div id="content">
      <div id="changeProjectName">
        <form  action="adminChangeProject.php" method="POST">
          <h3 id="writing"><strong>Change Project Name:</strong></h3> from
          <select name="project">
            <option value="blank"></option>
              <??><?php
                $projects = getProjects();
                foreach ($projects as $p){
                  echo '<option value="'.$p['name'].'">'.$p['name'].'</option>';
                }
              ?>
          </select>
          to <input type="text" name = "new_project_name" size="15">
          <input type="submit" value="Change" id="change_button"/>
        </form>
      </div>
      <br/>
      <div id="addNewAdmin" >
        <form action="adminNew.php" method="POST">
          <h3 id="writing"><strong>Add Admin:</strong></h3>
            <div id="admin_space">
              Username: <input type="text" name = "new_admin_name" size="15">
              <br/><br/>
              Password: <input type="password" name = "new_admin_pass" size="15">
              <br/><br/>
              Re-type Password: <input type="password" name = "new_admin_pass_check" size="15">
              <br/><br/>
              <input type="submit" value="Add" id="add_button"/>
            </div>
        </form>
      </div>
      <br/>
      <div id="deactivateProject">
        <form action="adminDeactivate.php" method="POST">
          <h3 id="writing"><strong> Deactivate Project: </strong></h3>
          <div id="admin_space">
            Select Project to Deactivate:
            <select name="project">
              <option value="blank"></option>
                <??><?php
                  $projects = getProjects();
                  foreach ($projects as $p){
                    echo '<option value="'.$p['name'].'">'.$p['name'].'</option>';
                  }
                ?>
            </select>
            <input type="submit" value="Deactivate" id="deactive_button"/>
          </div>
        </form>
      </div>

      <!-- DataTables CSS -->
      <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">

      <!-- jQuery -->
      <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>

      <!-- DataTables -->
      <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.js"></script>

      <div id="display" class="tracker_table_admin" style="display: none;">
        <h2 id="writing" style="float:left; padding-bottom: 1em;"><strong>New Additions Log</strong></h2>
        <table id="table_id" class="display">
          <thead>
              <tr>
                  <th>New Info</th>
                  <th>Time</th>
              </tr>
          </thead>
          <tbody>
              <??><?php
                $logInfo = getLogInfo();
                foreach ($logInfo as $l){
                  echo '<tr>';
                  if ($l['type'] == "na"){
                    echo '<td>Admin Added -- '. $l['add_name'].'</td>';
                  }
                  else if ($l['type'] == "ne"){
                    echo '<td>Employee Added -- '. $l['add_name'].'</td>';
                  }
                  else if ($l['type'] == "pc"){
                    echo '<td>Project Name Changed from '. $l['changed_from'].' to '.$l['add_name'].'</td>';
                  }
                  else if ($l['type'] == "de"){
                    echo '<td>Project Deactivated -- '. $l['add_name'].'</td>';
                  }
                  else{
                    echo '<td>Project Added -- '. $l['add_name'].'</td>';
                  }

                  echo '<td>'. $l['time'].'</td>';
                  echo '</tr>';
                }
            ?>
          </tbody>

          <script>
            $("#add_button").click( function (){
              alert("You have successfully created a new admin!");
            })

            $("#change_button").click( function (){
              alert("You have successfully changed the project name.");
            })

            $("#deactive_button").click( function (){
              alert("You have successfully deactivated the project.");
            })

            $(document).ready( function () {
            $('#table_id').dataTable( {
              "pageLength": 25,
              "order": [[1, "desc"]]});
            $('#display').show();
            });
            </script>
        </table>
      </div>
    </div>
  </body>
</html>
