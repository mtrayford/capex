<??>
<?php
  require '../dbConnection.php';
  $dbConn = getConnection();

  session_start();
  $_SESSION['project'] = $_POST['project'];
  $_SESSION['new_project_name'] = $_POST['new_project_name'];
  $project = $_SESSION['project'];
  $type = "pc";
  //pc = Project Changed

  $new_project_name = $_SESSION['new_project_name'];

  $sql = "UPDATE projects SET name= :new_project_name WHERE name = :project";
  $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $namedPara = array();
  $namedPara[':project'] = $project;
  $namedPara[':new_project_name'] = $new_project_name;
      $stmt = $dbConn->prepare($sql);
      $stmt->execute($namedPara);

  $sql = "INSERT INTO add_logs (type, add_name, changed_from)
    VALUES (:type, :new_project_name, :project)";
  $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $namedPara = array();
  $namedPara[':type'] = $type;
  $namedPara[':new_project_name'] = $new_project_name;
  $namedPara[':project'] = $project;
      $stmt = $dbConn->prepare($sql);
      $stmt->execute($namedPara);

  $url = "admin.php";
  header("Location: ". urlencode($url));
?>
