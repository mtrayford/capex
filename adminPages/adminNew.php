<??>
<?php
  require '../dbConnection.php';
  $dbConn = getConnection();
  session_start();

  if(!(empty($_POST['new_admin_name']))){
    if($_POST['new_admin_pass'] == $_POST['new_admin_pass_check']){

      $name = $_POST['new_admin_name'];
      $password = sha1($_POST['new_admin_pass']);
      $type = "na";
      // na = New Admin

      $sql = "INSERT INTO admin (username, password) VALUES (:name, :password)";
      $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $namedPara = array();
      $namedPara[':name'] = $name;
      $namedPara[':password'] = $password;
        $stmt = $dbConn->prepare($sql);
        $stmt->execute($namedPara);

        $sql = "INSERT INTO add_logs (type, add_name)
          VALUES (:type, :new_admin_name)";
        $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $namedPara = array();
        $namedPara[':type'] = $type;
        $namedPara[':new_admin_name'] = $name;
            $stmt = $dbConn->prepare($sql);
            $stmt->execute($namedPara);

        $url = "admin.php";
        header("Location: ". urlencode($url));
    }
  }

?>
