<??>
<?php
  require '../dbConnection.php';
  $dbConn = getConnection();

  session_start();
  $_SESSION['project'] = $_POST['project'];
  $project =  $_SESSION['project'];
  $type = "de";
  //de = deactivated

  $sql = "UPDATE projects SET status = 0 WHERE name = :project";
  $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $namedPara = array();
  $namedPara[':project'] = $project;
      $stmt = $dbConn->prepare($sql);
      $stmt->execute($namedPara);

  $sql = "INSERT INTO add_logs (type, add_name)
    VALUES (:type, :project)";
  $dbConn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $namedPara = array();
  $namedPara[':type'] = $type;
  $namedPara[':project'] = $project;
      $stmt = $dbConn->prepare($sql);
      $stmt->execute($namedPara);

  $url = "admin.php";
  header("Location: ". urlencode($url));
?>
