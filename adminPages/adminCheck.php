<?php
  session_start();
  require '../dbConnection.php';

  $dbConn = getConnection();

  $username = $_POST['username'];
  $password = sha1($_POST['password']);

  $sql = "SELECT * FROM admin WHERE username = :username AND password = :password";

  $namedParameters = array();
  $namedParameters[':username'] = $username;
  $namedParameters[':password'] = $password;
    $stmt = $dbConn -> prepare($sql);
    $stmt->execute($namedParameters);
    $result = $stmt->fetch();


  if (empty($result)) {
    $url = "index.php";
    header("Location: " . urlencode($url));
  }
  else{
    $url = "admin.php";
    header("Location: ". urlencode($url));
  }

?>
