<??><?php
  require '../dbConnection.php';
  $dbConn = getConnection();
  session_start();

  $username = $_POST['username'];
  $password = sha1($_POST['password']);

  $sql = "SELECT * FROM admin WHERE username = :username AND password = :password";
  $namedParameters = array();
  $namedParameters[':username'] = $username;
  $namedParameters[':password'] = $password;
    $stmt = $dbConn -> prepare($sql);
    $stmt->execute($namedParameters);
    $result = $stmt->fetch();

  if (empty($result)) {
    header("Location: ../index.php?error=WRONG USERNAME OR PASSWORD");
  }
  else {
    $_SESSION['username']  = $result['username'];
    $_SESSION['adminName'] = $result['firstName'] . " " . $result['lastName'];
    $url = "admin.php";
    header("Location: ". urlencode($url));
  }
?>
