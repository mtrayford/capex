<?php

require 'dbConnection.php';
require "functions.php";

 ?>

<html>
  <head>
    <title>CapEx Tracking: Search</title>
      <?php include 'headerContent.php'; ?>

      <!--DatePicker jQuery UI -->
      <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
      <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

      <!-- MultiDatesPicker API -->
      <script type="text/javascript" src="MultiDatesPicker v1.6.3/js/jquery-2.1.1.js"></script>
      <script type="text/javascript" src="MultiDatesPicker v1.6.3/js/jquery-ui-1.11.1.js"></script>
      <script type="text/javascript" src="MultiDatesPicker v1.6.3/jquery-ui.multidatespicker.js"></script>
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="destroy.php"><img src="https://www.languageline.com/images/languageline-logo.png"> Language Line Solutions</a>
        </div>
      </div><!--/.navbar-collapse -->
    </nav>
    <div id="editPage">

    <?php
      session_start();
      //var_dump($_POST);
      $_SESSION['date'] = $_POST['date'];
      $date = substr($_POST['date'], 3, 2);
      $date1 = date('m/d/Y', $_POST['date']);

      $_SESSION['hours'] = $_POST['hours'];
      $_SESSION['activity'] = $_POST['activity'];
    ?>
      <div id="edit_content">
        <form action="actionPages/updateEdit.php" method="POST">
          <h4><strong>Name: </strong></h4><h4><?= $_POST['name']?></h4>
          <input type="hidden" name="name" value="<?= $_POST['name']?>" />

          <h4><strong>Date: </strong></h4><input id="display-dates" type="text" name="new_dates"size="15" value="<?= $date1?>"/>
          <input type="hidden" name="date" value="<?= $_POST['date']?>" />
          <br/><br/>

          <script>
            var date = new Date();
            $( "#display-dates" ).multiDatesPicker({
  	        //   addDates: [date.setDate("<?= $date; ?>"), date.setDate(19)]
             });
          </script>

          <h4><strong>Hours: </strong></h4><input type="text" name = "new_hours" size="15" placeholder="<?= $_POST['hours']?>">

          <br/><br/>

          <h4><strong>Project: </strong></h4>
          <select name="new_project" id="dropdown" style="text-align:center;">
            <option value="blank"></option>
              <?php
                $projects = getProjects();
                foreach ($projects as $project){
                  echo '<option value="'.$project['name'].'">'.$project['name'].'</option>';
                }
              ?>
          </select>
          <br/><br/>
          <h4><strong>Activity: </strong></h4><input type="text" name = "new_activity" size="15" placeholder="<?= $_POST['activity']?>">
          <br/><br/>
          <input type="submit" value="Update" id="update_info"/>
          <form><INPUT Type="button" VALUE="Back" onClick="history.go(-1);return true;"></form>
        </form>
      </div>
    </div>
    <script>
      var project = "<?= $_POST['project'] ?>";
      $( document ).ready(function() {
        $("#dropdown").val(project);
      });
    </script>
  </body>
</html>
