<?php

	require_once 'dbConnection.php';
	session_start();
	include 'functions.php';
	getUsers();

	$pieData  = array();

	$pieData 	= getPieData(1);
?>

<!DOCTYPE html>
	<html>
		<head>
			<title>CapEx Tracking Tool</title>
			<?php include 'headerContent.php'; ?>
			<script type="text/javascript" src="../src/plugins/jqplot.barRenderer.min.js"></script>
			<script type="text/javascript" src="../src/plugins/jqplot.categoryAxisRenderer.min.js"></script>
			<script type="text/javascript" src="../src/plugins/jqplot.pointLabels.min.js"></script>
			<script src="//cdn.anychart.com/js/7.4.1/anychart-bundle.min.js"></script>
		</head>
		<body>
    	<nav class="navbar navbar-inverse navbar-fixed-top">
      	<div class="container-fluid">
        	<div class="navbar-header">
						  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						    <span class="sr-only">Toggle navigation</span>
						    <span class="icon-bar"></span>
						    <span class="icon-bar"></span>
						    <span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand" href="destroy.php"><img src="https://www.languageline.com/images/languageline-logo.png"> Language Line Solutions</a>
							<div class="login">
            		<form class="navbar-form navbar-right" role="search" action="adminPages/adminSecure.php" method="POST">
                	<div class="form-group">
                  	<input type="text" class="form-control" name="username" placeholder="Username">
                	</div>
                	<div class="form-group">
                  	<input type="password" class="form-control" name="password" placeholder="Password">
                	</div>
                  <button type="submit" class="btn btn-default">Sign In</button>
              	</form>
							</div>
        	</div>
      	</div><!--/.navbar-collapse -->
    	</nav>

			<div class="tracker">
  			<DIV TITLE="<?php include('css/rules.txt');?>" >
    			<h1>Capital Project Time Tracker
    			<a href="https://languageline.atlassian.net/wiki/display/IC/Capital+Time+Tracking" target="_blank">
      		<img src="http://web.nacm.org/images/question_mark_small.png" alt="question icon"/></a></h1>
      	</DIV>
				<div class="container1">
  				<form action="actionPages/enter.php" method="POST">
	   				Name:
	    			<select name="name">
		      	<option value="blank"></option>

            	<??><?php
              	$people = getUsers();
                	foreach ($people as $people){
                    	echo '<option value="'.$people['name'].'">'.$people['name'].'</option>';
                	}
            	?>
	    			</select>
	    			<br/><br/>

	  				Search:
	   				<input type="text" name = "porp" size="15">
    				<!-- porp == person or project -->
	  				<br/><br/>

	 					<input type="submit" value="Enter"/>

   					<br/><br/>
   					<br/><br/>
 					</form>

					<form action="actionPages/newName.php" method="POST">
   					Add New Employee:
      			<input type="text" name = "add_name" size="15">
      			<input type="submit" value="Add" id="add_new_name"/>
					</form>

  				<br/><br/>

 					<form action="actionPages/newProject.php" method="POST">
  					Add New Project:
  					<input type="text" name = "add_project" size="15">
  					<input type="submit" value="Add" id="add_new_project"/>
 					</form>
				</div>
			 <div id="chartdiv" style="height:71em !important;width:100%; !important"></div>
			</div>
		</body>

		<script type="text/javascript">
  		$("#add_new_name").click( function (){
    		alert("You have added to the list of users.");
  		})

  		$("#add_new_project").click( function (){
    		alert("You have added to the list of projects.");
  		})
		</script>

		<script type="text/javascript">

			anychart.onDocumentReady(function() {

			//create pie chart with passed data
			chart = anychart.pie([
				["<?php echo $pieData[0][0] ?>", "<?php echo $pieData[0][1] ?>"],
				["<?php echo $pieData[1][0] ?>", "<?php echo $pieData[1][1] ?>"],
				["<?php echo $pieData[2][0] ?>", "<?php echo $pieData[2][1] ?>"],
				["<?php echo $pieData[3][0] ?>", "<?php echo $pieData[3][1] ?>"],
				["<?php echo $pieData[4][0] ?>", "<?php echo $pieData[4][1] ?>"]
			]);

			chart.palette(['#3F5CA9', '#1AA1E1','#B3C833', '#FB892A', '#CE5043', 'FFCA0E']);
			chart.stroke(null);
			chart.bounds(0, "50%", "100%", "50%");
			//set container id for the chart
			chart.container('chartdiv');

			//set chart title text settings
			chart.title().text('Hours Completed by Project');

			//set chart predefined aqua style
			chart.fill('aquaStyle');

			chart.group(function(value) {
				return (value > 0);
			});

			//initiate chart drawing
			chart.draw();
		});
		</script>
</html>
